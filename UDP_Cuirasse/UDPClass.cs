﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace UDP_Cuirasse
{
    /// <summary>
    /// UDP class.
    /// </summary>
    public class UDPClass : UdpCuirasseModel
    {
        /// <summary>
        /// Instance of class
        /// </summary>
        private UdpClient udp;

        /// <summary>
        /// Local ip address.
        /// </summary>
        private string localIpAddress;

        /// <summary>
        /// Local port.
        /// </summary>
        private int localPort;

        /// <summary>
        /// Remote ip address.
        /// </summary>
        private string remoteIpAddress;

        /// <summary>
        /// Remote port.
        /// </summary>
        private int remotePort;

        private List<byte[]> Buffer { get; set; } = new List<byte[]>();

        /// <summary>
        /// The list for save spectrum version 1 or 2.
        /// </summary>
        private List<byte[]> listForSave = new List<byte[]>();

        /// <summary>
        /// The list for save correlation version 1 or 2.
        /// </summary>
        private List<byte[]> listForSaveCorr = new List<byte[]>();

        /// <summary>
        /// ReadData Thread
        /// </summary>
        private Thread ReadThread { get; set; } = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="UDPClass"/> class.
        /// </summary>
        public UDPClass()
        {
            this.localIpAddress = "127.0.0.1";
            this.localPort = 15000;
            this.remoteIpAddress = "127.0.0.1";
            this.remotePort = 15001;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UDPClass"/> class.
        /// </summary>
        /// <param name="localIp">
        /// The local ip.
        /// </param>
        /// <param name="localPort">
        /// The local port.
        /// </param>
        /// <param name="remoteIp">
        /// The remote ip.
        /// </param>
        /// <param name="remotePort">
        /// The remote port.
        /// </param>
        /// <param name="addrSender">
        /// The address sender for information field.
        /// </param>
        /// <param name="addrRecipient">
        /// The address recipient for information field.
        /// </param>
        /// <param name="typeWorkFlag">
        /// The type work flag.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        public UDPClass(string localIp, int localPort, string remoteIp, int remotePort, byte addrSender, byte addrRecipient)
        {
            this.localIpAddress = localIp;
            this.localPort = localPort;
            this.remoteIpAddress = remoteIp;
            this.remotePort = remotePort;
            this.AddrRecipient = addrRecipient;
            this.AddrSender = addrSender;
        }

        /// <summary>
        /// Event to receive the sent array of bytes.
        /// </summary>
        public override event EventHandler<byte[]> OnSendBytes;

        /// <summary>
        /// Event to receive the receive array of bytes.
        /// </summary>
        public override event EventHandler<byte[]> OnReceiveBytes;
        
        /// <summary>
        /// Event for receiving data on drones from the recognition system.
        /// </summary>
        public override event EventHandler<DataRecognitionSystemEventArgs> OnDataFromRecognitionSystem;

        /// <summary>
        /// Address of the recipient.
        /// </summary>
        protected override byte AddrRecipient { get; set; }

        /// <summary>
        /// Sender address.
        /// </summary>
        protected override byte AddrSender { get; set; }

        /// <summary>
        /// Codogram counter.
        /// </summary>
        protected override byte CountCmd { get; set; }

        /// <summary>
        /// UDP connect.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool Connect()
        {
            try
            {
                if(udp == null)
                {
                    this.udp = new UdpClient();
                    this.udp.Connect(this.remoteIpAddress, this.remotePort);
                }
                
                if(ReadThread == null)
                {
                    ReadThread = new Thread(ReadData);
                    ReadThread.IsBackground = true;
                    ReadThread.Start();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Close UDP connect.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public override bool Disconnect()
        {
            try
            {
                if (ReadThread != null)
                {
                    ReadThread.Abort();
                    ReadThread.Join(500);
                    ReadThread = null;
                }

                if (udp != null)
                {
                    this.udp.Close();
                    this.udp.Dispose();
                    this.udp = null;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
        
        /// <summary>
        /// Set wait for some functions.
        /// </summary>
        /// <param name="millisecondsDelay">
        /// The milliseconds delay.
        /// </param>
        public override void SetWaitTime(int millisecondsDelay)
        {
            this.WaitTime = millisecondsDelay;
        }

        /// <summary>
        /// ReadData while in thread
        /// </summary>
        private void ReadData()
        {
            UdpClient udpReceiver = new UdpClient(localPort);
            IPEndPoint remoteIp = null;

            while (true)
            {
                byte[] receiveBytes = udpReceiver.Receive(ref remoteIp);

                if(receiveBytes != null)
                {
                    SendToBuffer(receiveBytes);
                }
            }
        }

        /// <summary>
        /// Add to Buffer & try to parse
        /// </summary>
        /// <param name="receiveBytes">Received bytes</param>
        private void SendToBuffer(byte[] receiveBytes)
        {
            Buffer.Add(receiveBytes);
            TryParse();
            return;
        }

        /// <summary>
        /// Try to Parse data from Buffer
        /// </summary>
        private void TryParse()
        {
            byte[] bytes = Buffer.FirstOrDefault();
            int lengthCodogram = GetLength(bytes) + 8; // 8 - Header Length

            if(bytes.Length == lengthCodogram)
            {
                //parse and delete bytes from buffer
                DecodeReceiveData(bytes);
                Buffer.RemoveAt(0);
            }
            else if(bytes.Length > lengthCodogram)
            {
                //cut bytes and parse, we need to save remaining bytes and delete parsed bytes
                byte[] smallerArray = new byte[lengthCodogram];
                Array.Copy(bytes, 0, smallerArray, 0, lengthCodogram);

                DecodeReceiveData(smallerArray);

                Buffer.RemoveAt(0);

                byte[] remainingBytes = new byte[bytes.Length - lengthCodogram];
                Array.Copy(bytes, lengthCodogram, remainingBytes, 0, remainingBytes.Length);
                Buffer.Insert(0, remainingBytes);
            }
            else if(bytes.Length < lengthCodogram)
            {
                // need to take as many bytes so that there is enough length for parsing, save the remaining data
                byte[] largerArray = new byte[lengthCodogram];
                int currentLength = 0;

                for( ; ; )
                {
                    if(currentLength + bytes.Length > lengthCodogram)
                    {
                        Array.Copy(bytes, 0, largerArray, currentLength, currentLength + bytes.Length - lengthCodogram);
                        Buffer.RemoveAt(0);

                        byte[] remainingBytes = new byte[currentLength + bytes.Length - lengthCodogram];
                        Array.Copy(bytes, remainingBytes.Length, remainingBytes, 0, remainingBytes.Length);
                        Buffer.Insert(0, remainingBytes);
                        break;
                    }

                    Array.Copy(bytes, 0, largerArray, 0, bytes.Length);
                    currentLength += bytes.Length;

                    Buffer.RemoveAt(0);

                    if(currentLength == lengthCodogram)
                    {
                        break;
                    }

                    bytes = Buffer.FirstOrDefault();

                    if(bytes == null)
                    {
                        Buffer.Clear();
                        return;
                    }
                }

                DecodeReceiveData(largerArray);
            }

            return;
        }

        /// <summary>
        /// Check length of codogram
        /// </summary>
        /// <param name="intLength">Length of info field</param>
        /// <returns>Converted to int length</returns>
        private int GetLength(byte[] intLength)
        {
            byte[] intArray = new byte[4];
            Array.Copy(intLength, 4, intArray, 0, 4);
            string hexString = string.Join(string.Empty, Array.ConvertAll(intArray, b => b.ToString("X")));
            return Convert.ToInt32(hexString, 16);
        }

        /// <summary>
        /// Status query.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task<GetStateEventArgs> GetState(CancellationToken token = default)
        {
            try
            {
                this.State = null;
                const int CodogramaLength = 8;
                var bytesForSend = this.BaseCmd(CodogramaLength, (byte)CodogramCiphers.REQUEST_STATE);

                await this.Send(bytesForSend, token).ConfigureAwait(false);

                do
                {
                    await Task.Delay(1).ConfigureAwait(false);

                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    if (stopwatch.ElapsedMilliseconds > WaitTime)
                    {
                        break;
                    }
                }
                while (State == null);

                return State;

            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Set parameters (frequency).
        /// </summary>
        /// <param name="freq">
        /// Freq.
        /// </param>
        /// <param name="gain">
        /// Gain.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task<SetFreqEventArgs> SetFreq(int freq, byte gain, CancellationToken token = default)
        {
            try
            {
                this.Freq = null;

                const int CodogramaLength = 13;
                var bytesForSend = this.BaseCmd(CodogramaLength, (byte)CodogramCiphers.SET_FREQ);

                bytesForSend[8] = (byte)(freq & 0xFF);
                bytesForSend[9] = (byte)((freq >> 8) & 0xFF);
                bytesForSend[10] = (byte)((freq >> 16) & 0xFF);
                bytesForSend[11] = (byte)((freq >> 24) & 0xFF);
                bytesForSend[12] = Convert.ToByte(gain);

                await this.Send(bytesForSend, token).ConfigureAwait(false);

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                do
                {
                    await Task.Delay(1).ConfigureAwait(false);

                    if (stopwatch.ElapsedMilliseconds > WaitTime)
                    {
                        break;
                    }
                }
                while (Freq == null);

                return Freq;

            }
            catch
            {
                return null;
            }
        }
        
        /// <summary>
        /// Spectrum request.
        /// </summary>
        /// <param name="itemNumber">
        /// The item number.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task<GetSpectrumEventArgs> GetSpectrum(byte itemNumber, CancellationToken token = default)
        {
            try
            {
                this.Spectrum = null;

                if(listForSave.Count > 0)
                {
                    listForSave.Clear();
                }

                const int CodogramaLength = 9;
                var bytesForSend = this.BaseCmd(CodogramaLength, (byte)CodogramCiphers.REQUEST_SPECTRUM);

                bytesForSend[8] = itemNumber;

                await this.Send(bytesForSend, token).ConfigureAwait(false);

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                do
                {
                    await Task.Delay(1).ConfigureAwait(false);

                    if (stopwatch.ElapsedMilliseconds > WaitTime)
                    {
                        break;
                    }
                }
                while (Spectrum == null);

                return Spectrum;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Setting the parameters of the correlation function.
        /// </summary>
        /// <param name="bias">
        /// The bias.
        /// </param>
        /// <param name="filterValue">
        /// The filter value MHz.
        /// </param>
        /// <param name="alfa1">
        /// The alfa 1.
        /// </param>
        /// <param name="alfa2">
        /// The alfa 2.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task<SetParamEventArgs> SetParam(int bias, byte filterValue, int alfa1, int alfa2, CancellationToken token = default)
        {
            try
            {
                this.SetParamF = null;

                const int CodogramaLength = 21;
                var bytesForSend = this.BaseCmd(CodogramaLength, (byte)CodogramCiphers.SET_PARAM);

                var a = BitConverter.GetBytes(bias);
                bytesForSend[8] = filterValue;
                bytesForSend[9] = a[0];
                bytesForSend[10] = a[1];
                bytesForSend[11] = a[2];
                bytesForSend[12] = a[3];

                a = BitConverter.GetBytes(alfa1);
                bytesForSend[13] = a[0];
                bytesForSend[14] = a[1];
                bytesForSend[15] = a[2];
                bytesForSend[16] = a[3];

                a = BitConverter.GetBytes(alfa2);
                bytesForSend[17] = a[0];
                bytesForSend[18] = a[1];
                bytesForSend[19] = a[2];
                bytesForSend[20] = a[3];

                await this.Send(bytesForSend, token).ConfigureAwait(false);

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                do
                {
                    await Task.Delay(1).ConfigureAwait(false);

                    if (stopwatch.ElapsedMilliseconds > WaitTime)
                    {
                        break;
                    }
                }
                while (SetParamF == null);

                return SetParamF;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Correlation function query version one.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task<GetCorrelationEventArgs> GetCorrelationFunc(CancellationToken token = default)
        {
            try
            {
                this.Correlation = null;

                if(listForSaveCorr.Count > 0)
                {
                    listForSaveCorr.Clear();
                }

                const int CodogramaLength = 8;
                var bytesForSend = this.BaseCmd(CodogramaLength, (byte)CodogramCiphers.REQUEST_CORRELATION_FUNCTION);

                await this.Send(bytesForSend, token).ConfigureAwait(false);

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                do
                {
                    await Task.Delay(1).ConfigureAwait(false);

                    if (stopwatch.ElapsedMilliseconds > WaitTime)
                    {
                        break;
                    }
                }
                while (Correlation == null);

                return Correlation;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The start stop.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task<StartStopEventArgs> Start_Stop(START_STOP value, CancellationToken token = default)
        {
            try
            {
                this.StartStop = null;

                const int CodogramaLength = 9;
                var bytesForSend = this.BaseCmd(CodogramaLength, (byte)CodogramCiphers.START_STOP);

                bytesForSend[8] = (byte)value;

                await this.Send(bytesForSend, token).ConfigureAwait(false);

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                do
                {
                    await Task.Delay(1).ConfigureAwait(false);

                    if (stopwatch.ElapsedMilliseconds > WaitTime)
                    {
                        break;
                    }
                }
                while (StartStop == null);

                return StartStop;
            }
            catch
            {
                return StartStop;
            }
        }

        /// <summary>
        /// Recognition request.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task<SetParamEventArgs> RecognitionRequest(CancellationToken token = default)
        {
            try
            {
                this.Recognition = null;

                const int CodogramaLength = 8;
                var bytesForSend = this.BaseCmd(CodogramaLength, (byte)CodogramCiphers.RECOGNITION_REQUEST);

                await this.Send(bytesForSend, token).ConfigureAwait(false);

                var stopwatch = new Stopwatch();
                stopwatch.Start();
                do
                {
                    await Task.Delay(1).ConfigureAwait(false);

                    if (stopwatch.ElapsedMilliseconds > WaitTime)
                    {
                        break;
                    }
                }
                while (Recognition == null);

                return Recognition;
            }
            catch
            {
                return Recognition;
            }
        }

        /// <summary>
        /// Send datagram via udp.
        /// </summary>
        /// <param name="arrayBytes">
        /// The array bytes.
        /// </param>
        public override async Task Send(byte[] arrayBytes, CancellationToken token = default)
        {
            try
            {
                await this.udp.SendAsync(arrayBytes, arrayBytes.Length);
                OnSendBytes?.Invoke(this, arrayBytes);
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Decoder of receive data.
        /// </summary>
        /// <param name="receiveBytes">
        /// The receive bytes.
        /// </param>
        protected override void DecodeReceiveData(byte[] receiveBytes)
        {
            OnReceiveBytes?.Invoke(this, receiveBytes);

            try
            {
                switch (receiveBytes[2])
                {
                    case 1 when receiveBytes.Length == 11:
                        {
                            State = new GetStateEventArgs(
                                (CodogramCiphers)receiveBytes[2],
                                (ErrorCode)receiveBytes[8],
                                (Optic)receiveBytes[9],
                                receiveBytes[10]);
                            break;
                        }

                    case 2 when receiveBytes.Length == 9:
                        {
                            Freq = new SetFreqEventArgs((CodogramCiphers)receiveBytes[2], (ErrorCode)receiveBytes[8]);
                            break;
                        }
                    case 5 when receiveBytes.Length == 9:
                        {
                            SetParamF = new SetParamEventArgs(CodogramCiphers.SET_PARAM, (ErrorCode)receiveBytes[8]);
                            break;
                        }

                    case 6 when receiveBytes.Length == 9:
                        {
                            StartStop = new StartStopEventArgs(CodogramCiphers.START_STOP, (START_STOP)receiveBytes[8]);
                            break;
                        }

                    case 7 when receiveBytes.Length == 9:
                        {
                            Recognition = new SetParamEventArgs((CodogramCiphers)receiveBytes[2], (ErrorCode)receiveBytes[8]);
                            break;
                        }
                    case 3:
                    case 4:
                    case 8:
                        {
                            OnReceivingMultiplePackets(receiveBytes);
                            break;
                        }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Decoding codegrams that come in several packages
        /// </summary>
        /// <param name="receiveBytes"></param>
        private void OnReceivingMultiplePackets(byte[] receiveBytes)
        {
            switch (receiveBytes[2])
            {
                case 3 when receiveBytes.Length > 20:
                    {
                        var spectrumDictionary = new Dictionary<byte, byte[]>();
                        this.listForSave.Add(receiveBytes);
                        if (this.listForSave.Count - 1 == receiveBytes[11])
                        {
                            foreach (var item in this.listForSave)
                            {
                                this.DecoderSpectrum(item, spectrumDictionary);
                            }
                        }

                        break;
                    }

                case 4 when receiveBytes.Length > 20:
                    {
                        var corrDictionary = new Dictionary<byte, byte[]>();
                        this.listForSaveCorr.Add(receiveBytes);
                        if (this.listForSaveCorr.Count - 1 == receiveBytes[10])
                        {
                            foreach (var item in this.listForSaveCorr)
                            {
                                DecoderCorr(item, corrDictionary);
                            }
                        }

                        break;
                    }

                case 8:
                    {
                        var recognitionList = new List<DataRecognitionSystem>();

                        for (var i = 9; i < receiveBytes.Length - 1; i += 18)
                        {
                            recognitionList.Add(new DataRecognitionSystem(BitConverter.ToInt32(receiveBytes, i), receiveBytes[i + 4], receiveBytes[i + 5], BitConverter.ToDouble(receiveBytes, i + 6), ToFloatNo(receiveBytes, i + 14)));
                        }

                        OnDataFromRecognitionSystem?.Invoke(this, new DataRecognitionSystemEventArgs((ErrorCode)receiveBytes[8], recognitionList));

                        break;
                    }
            }
        }

        /// <summary>
        /// Decoder of spectrum.
        /// </summary>
        /// <param name="byteArray">
        /// The byte array.
        /// </param>
        /// <param name="spectrumDictionary">
        /// The spectrum dictionary.
        /// </param>
        private void DecoderSpectrum(byte[] byteArray, Dictionary<byte, byte[]> spectrumDictionary)
        {
            if (byteArray.Length == 0)
            {
                return;
            }

            try
            {
                var intermediateArray = new byte[byteArray.Length - 12];
                Array.Copy(byteArray, 12, intermediateArray, 0, intermediateArray.Length);
                spectrumDictionary.Add(byteArray[10], intermediateArray);
                if (spectrumDictionary.Count - 1 != byteArray[11])
                {
                    return;
                }

                spectrumDictionary = spectrumDictionary.OrderBy(obj => obj.Key)
                    .ToDictionary(obj => obj.Key, obj => obj.Value);
                var listSpectrum = new List<float>();

                foreach (var a in spectrumDictionary.Values)
                {
                    for (var i = 0; i < a.Length; i += 4)
                    {
                        listSpectrum.Add(ToFloatNo(a, i));
                    }
                }

                var lengthInfo = new byte[] { byteArray[4], byteArray[5], byteArray[6], byteArray[7] };
                this.Spectrum = new GetSpectrumEventArgs(byteArray[11], lengthInfo, (CodogramCiphers)byteArray[2], (ErrorCode)byteArray[8], (ItemNumber)byteArray[9], new List<float> (listSpectrum), 1);
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Decoder correlation version 1 or 2.
        /// </summary>
        /// <param name="byteArray">
        /// The byte array.
        /// </param>
        /// <param name="corrDictionary">
        /// The corr dictionary.
        /// </param>
        private void DecoderCorr(byte[] byteArray, Dictionary<byte, byte[]> corrDictionary)
        {
            try
            {
                var intermediateArray = new byte[byteArray.Length - 11];
                Array.Copy(byteArray, 11, intermediateArray, 0, intermediateArray.Length);
                corrDictionary.Add(byteArray[9], intermediateArray);
                if (corrDictionary.Count - 1 == byteArray[10])
                {
                    corrDictionary = corrDictionary.OrderBy(obj => obj.Key).ToDictionary(obj => obj.Key, obj => obj.Value);
                    var listCorr = new List<Correlation>();
                    foreach (var a in corrDictionary.Values)
                    {
                        var l = a.Length / 2;

                        var startCorr = BitConverter.ToDouble(a, 0);
                        var corr = new List<double>();
                        var endCorr = BitConverter.ToDouble(a, l - 8);

                        for (var i = 8; i < l - 15; i += 8)
                        {
                            corr.Add(BitConverter.ToDouble(a, i));
                        }

                        listCorr.Add(new Correlation(startCorr, new List<double>(corr), endCorr));

                        corr.Clear();

                        startCorr = BitConverter.ToDouble(a, l);
                        endCorr = BitConverter.ToDouble(a, a.Length - 8);

                        for (var i = l + 8; i < a.Length - 8; i += 8)
                        {
                            corr.Add(BitConverter.ToDouble(a, i));
                        }

                        listCorr.Add(new Correlation(startCorr, new List<double>(corr), endCorr));
                    }
                    var lengthInfo = new[] { byteArray[4], byteArray[5], byteArray[6], byteArray[7] };
                    this.Correlation = new GetCorrelationEventArgs(byteArray[10], lengthInfo, (CodogramCiphers)byteArray[2], (ErrorCode)byteArray[8], new List<Correlation>(listCorr), 1);
                }
                else
                {
                    return;
                }
            }
            catch
            {
                // ignored
            }
        }
    }
}
