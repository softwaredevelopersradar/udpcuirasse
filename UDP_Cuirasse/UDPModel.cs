﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace UDP_Cuirasse
{
    public abstract class UdpModel
    {
        /// <summary>
        /// Event to receive the sent array of bytes.
        /// </summary>
        public abstract event EventHandler<byte[]> OnSendBytes;

        /// <summary>
        /// Event to receive the receive array of bytes.
        /// </summary>
        public abstract event EventHandler<byte[]> OnReceiveBytes;
        
        /// <summary>
        /// Open UDP type connection
        /// </summary>
        /// <returns>True - connect, False - disconnect</returns>
        public abstract bool Connect();

        /// <summary>
        /// Disconnect UDP
        /// </summary>
        /// <returns>True - connect, False - disconnect</returns>
        public abstract bool Disconnect();

        /// <summary>
        /// Send some byte array via Udp
        /// </summary>
        /// <param name="arrayBytes"> Datagram </param>
        public abstract Task Send(byte[] arrayBytes, CancellationToken token = default);

    }
}
