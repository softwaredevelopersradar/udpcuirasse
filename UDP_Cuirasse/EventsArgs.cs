﻿using System;
using System.Collections.Generic;

namespace UDP_Cuirasse
{
    public class AmplitudeEventArgs : EventArgs
    {
        public byte Number { get; private set; }

        public AmplitudeEventArgs()
        {
            Number = 0;
        }

        public AmplitudeEventArgs(byte Number)
        {
            this.Number = Number;
        }
    }

    public class AmplitudeIntEventArgs : EventArgs
    {
        public int Ampl { get; private set; }

        public AmplitudeIntEventArgs()
        {
            Ampl = 0;
        }

        public AmplitudeIntEventArgs(int Ampl)
        {
            this.Ampl = Ampl;
        }
    }

    public class ByteEventArgs : EventArgs
    {
        public byte Data { get; private set; }

        public ByteEventArgs()
        {
            Data = 0;
        }

        public ByteEventArgs(byte Data)
        {
            this.Data = Data;
        }
    }

    public class CipherEventArgs : EventArgs
    {
        public CodogramCiphers Ciphers { get; private set; }

        public CipherEventArgs()
        {
            Ciphers = 0;
        }

        public CipherEventArgs(CodogramCiphers Ciphers)
        {
            this.Ciphers = Ciphers;
        }
    }

    public class ErrorEventArgs : EventArgs
    {
        public ErrorCode Error { get; private set; }

        public ErrorEventArgs()
        {
            Error = 0;
        }

        public ErrorEventArgs(ErrorCode Error)
        {
            this.Error = Error;
        }
    }

    public class FirmwareEventArgs : EventArgs
    {
        public byte Version { get; private set; }

        public FirmwareEventArgs()
        {
            Version = 0;
        }

        public FirmwareEventArgs(byte Version)
        {
            this.Version = Version;
        }
    }
    
    public class GetCorrelationEventArgs : EventArgs
    {
        public byte CountOfPackage { get; set; }
        public byte[] Inf { get; private set; }
        public ErrorCode Error { get; private set; }
        public CodogramCiphers Ciphers { get; private set; }
        public List<Correlation> Corr { get; private set; }
        public byte Version { get; private set; }

        public GetCorrelationEventArgs()
        {
            CountOfPackage = 0;
            Inf = null;
            Ciphers = CodogramCiphers.REQUEST_CORRELATION_FUNCTION;
            Error = ErrorCode.SUCCESSFULLY;
            Corr = null;
            Version = 1;
        }

        public GetCorrelationEventArgs(GetCorrelationEventArgs getCorrelation)
        {
            this.CountOfPackage = getCorrelation.CountOfPackage;
            this.Inf = getCorrelation.Inf;
            this.Ciphers = getCorrelation.Ciphers;
            this.Error = getCorrelation.Error;
            this.Corr = getCorrelation.Corr;
            this.Version = getCorrelation.Version;
        }

        public GetCorrelationEventArgs(byte CountOfPackage, byte[] Inf, CodogramCiphers Ciphers, ErrorCode Error, List<Correlation> Corr, byte Version)
        { 
            this.CountOfPackage = CountOfPackage;
            this.Inf = Inf;
            this.Ciphers = Ciphers;
            this.Error = Error;
            this.Corr = Corr;
            this.Version = Version;
        }
    }

    
    public class GetSpectrumEventArgs : EventArgs
    {
        public byte CountOfPackage { get; private set; }
        public byte[] Inf { get; private set; }
        public ErrorCode Error { get; private set; }
        public CodogramCiphers Ciphers { get; private set; }
        public ItemNumber Number { get; private set; }
        public List<float> Ampl { get; private set; }
        public byte Version { get; private set; }

        public GetSpectrumEventArgs()
        { }

        public GetSpectrumEventArgs(GetSpectrumEventArgs getSpectrum)
        {
            this.CountOfPackage = getSpectrum.CountOfPackage;
            this.Inf = getSpectrum.Inf;
            this.Ciphers = getSpectrum.Ciphers;
            this.Error = getSpectrum.Error;
            this.Number = getSpectrum.Number;
            this.Ampl = getSpectrum.Ampl;
            this.Version = getSpectrum.Version;
        }

        public GetSpectrumEventArgs(byte CountOfPackage, byte[] Inf, CodogramCiphers Ciphers, ErrorCode Error, ItemNumber Number, List<float> Ampl, byte Version)
        {
            this.CountOfPackage = CountOfPackage;
            this.Inf = Inf;
            this.Ciphers = Ciphers;
            this.Error = Error;
            this.Number = Number;
            this.Ampl = Ampl;
            this.Version = Version;
        }
    }

    public class GetStateEventArgs : EventArgs
    {
        public ErrorCode Error { get; private set; }
        public Optic State { get; private set; }
        public CodogramCiphers Ciphers { get; private set; }
        public byte Version { get; private set; }

        public GetStateEventArgs()
        {
            Ciphers = 0;
            Error = 0;
            State = 0;
            Version = 0;
        }
        public GetStateEventArgs(CodogramCiphers Ciphers, ErrorCode Error, Optic State, byte Version)
        {
            this.Ciphers = Ciphers;
            this.Error = Error;
            this.State = State;
            this.Version = Version;
        }
    }

    public class StartStopEventArgs : EventArgs
    {
        public CodogramCiphers Ciphers { get; private set; }
        public START_STOP State { get; private set; }

        public StartStopEventArgs()
        {
            Ciphers = (CodogramCiphers)6;
            State = 0;
        }
        public StartStopEventArgs(CodogramCiphers Ciphers, START_STOP State)
        {
            this.Ciphers = Ciphers;
            this.State = State;
        }
    }

    public class InfEventArgs : EventArgs
    {
        public byte Lenth { get; private set; }

        public InfEventArgs(byte Lenth)
        {
            this.Lenth = Lenth;
        }
    }

    public class ItemNumberEventArgs : EventArgs
    {
        public ItemNumber Number { get; private set; }

        public ItemNumberEventArgs()
        {
            Number = 0;
        }

        public ItemNumberEventArgs(ItemNumber Number)
        {
            this.Number = Number;
        }
    }

    public class OpenCloseEventArgs : EventArgs
    {
        public bool Flag { get; private set; }

        public OpenCloseEventArgs()
        {
            Flag = true;
        }

        public OpenCloseEventArgs(bool Flag)
        {
            this.Flag = Flag;
        }
    }

    public class ReceiveEventArgs : EventArgs
    {
        public byte[] Bytes { get; private set; }

        public ReceiveEventArgs()
        {
            Bytes = new byte[0];
        }
        public ReceiveEventArgs(byte[] receiveDatagram)
        {
            Bytes = receiveDatagram;
        }
    }

    public class SendEventArgs : EventArgs
    {
        public byte[] Bytes { get; private set; }

        public SendEventArgs()
        {
            Bytes = new byte[0];
        }
        public SendEventArgs(byte[] sendDatagram)
        {
            Bytes = sendDatagram;
        }
    }

    public class SetFreqEventArgs : EventArgs
    {
        public ErrorCode Error { get; private set; }
        public CodogramCiphers Ciphers { get; private set; }

        public SetFreqEventArgs()
        {
            Ciphers = 0;
            Error = 0;
        }

        public SetFreqEventArgs(CodogramCiphers Ciphers, ErrorCode Error)
        {
            this.Ciphers = Ciphers;
            this.Error = Error;
        }
    }

    public class SetParamEventArgs : EventArgs
    {
        public ErrorCode Error { get; private set; }
        public CodogramCiphers Ciphers { get; private set; }

        public SetParamEventArgs()
        {
            Ciphers = 0;
            Error = 0;
        }

        public SetParamEventArgs(CodogramCiphers Ciphers, ErrorCode Error)
        {
            this.Ciphers = Ciphers;
            this.Error = Error;
        }
    }

    public class DataEvenetArgs : EventArgs
    {
        public List<List<double>> List { get; private set; } = new List<List<double>>();
        public DataEvenetArgs(List<List<double>> List)
        {
            this.List = List;
        }
    }

    public class OpticEventArgs : EventArgs
    {
        public Optic State { get; private set; }

        public OpticEventArgs()
        {
            State = 0;
        }

        public OpticEventArgs(Optic State)
        {
            this.State = State;
        }
    }

    public class Correlation
    {
        public double sCorr { get; set; }
        public List<double> Corr { get; set; }
        public double eCorr { get; set; }

        public Correlation(double sCorr, List<double> Corr, double eCorr)
        {
            this.sCorr = sCorr;
            this.Corr = Corr;
            this.eCorr = eCorr;
        }
    }

    public class DataRecognitionSystem
    {
        public int ID { get; set; }
        public byte Type { get; set; }
        public byte Affiliation { get; set; }
        public double FreqKHz { get; set; }
        public float BandWidthMHz { get; set; }

        public DataRecognitionSystem()
        {
            ID = 0;
            Type = 0;
            FreqKHz = 0;
            Affiliation = 0;
            BandWidthMHz = 0;
        }

        public DataRecognitionSystem(int ID, byte Type, byte Affiliation, double FreqKHz, float BandWidthMHz)
        {
            this.ID = ID;
            this.Type = Type;
            this.FreqKHz = FreqKHz;
            this.Affiliation = Affiliation;
            this.BandWidthMHz = BandWidthMHz;
        }
    }

    public class DataRecognitionSystemEventArgs : EventArgs
    {
        public CodogramCiphers Cipher { get; private set; }
        public ErrorCode Error { get; private set; }
        public List<DataRecognitionSystem> Data { get; private set; }

        public DataRecognitionSystemEventArgs(ErrorCode Error, List<DataRecognitionSystem> Data)
        {
            Cipher = CodogramCiphers.DATA_FROM_RECOGNITION_SYSTEM;
            this.Error = Error;
            this.Data = Data;
        }
    }
}
