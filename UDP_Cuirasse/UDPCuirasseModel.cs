﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace UDP_Cuirasse
{
    public abstract class UdpCuirasseModel : UdpModel
    {
        /// <summary>
        /// Drone data from the recognition system.
        /// </summary>
        public abstract event EventHandler<DataRecognitionSystemEventArgs> OnDataFromRecognitionSystem;

        /// <summary>
        /// The spectrum.
        /// </summary>
        protected GetSpectrumEventArgs Spectrum;

        /// <summary>
        /// The correlation.
        /// </summary>
        protected GetCorrelationEventArgs Correlation;

        /// <summary>
        /// The state.
        /// </summary>
        protected GetStateEventArgs State;

        /// <summary>
        /// The freq.
        /// </summary>
        protected SetFreqEventArgs Freq;

        /// <summary>
        /// The start.
        /// </summary>
        protected StartStopEventArgs StartStop;

        /// <summary>
        /// The recognition.
        /// </summary>
        protected SetParamEventArgs Recognition;

        /// <summary>
        /// The set param f.
        /// </summary>
        protected SetParamEventArgs SetParamF;

        /// <summary>
        /// The wait time.
        /// </summary>
        protected int WaitTime = 5000;

        /// <summary>
        /// Gets or sets the addr sender.
        /// </summary>
        protected abstract byte AddrSender { get; set; }

        /// <summary>
        /// Gets or sets the addr recipient.
        /// </summary>
        protected abstract byte AddrRecipient { get; set; }

        /// <summary>
        /// Gets or sets the count cmd.
        /// </summary>
        protected abstract byte CountCmd { get; set; }

        /// <summary>
        /// The wa set wait time.
        /// </summary>
        /// <param name="millisecondsDelay">
        /// The milliseconds delay.
        /// </param>
        public abstract void SetWaitTime(int millisecondsDelay);

        /// <summary>
        /// Status query.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public abstract Task<GetStateEventArgs> GetState(CancellationToken token = default);

        /// <summary>
        /// Set parameters (frequency).
        /// </summary>
        /// <param name="freq">
        /// Freq.
        /// </param>
        /// <param name="gain">
        /// Gain.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public abstract Task<SetFreqEventArgs> SetFreq(int freq, byte gain, CancellationToken token = default);

        /// <summary>
        /// Spectrum request.
        /// </summary>
        /// <param name="itemNumber">
        /// The item number.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public abstract Task<GetSpectrumEventArgs> GetSpectrum(byte itemNumber, CancellationToken token = default);

        /// <summary>
        /// Setting the parameters of the correlation function.
        /// </summary>
        /// <param name="bias">
        /// The bias.
        /// </param>
        /// <param name="filterValue">
        /// The filter value MHz.
        /// </param>
        /// <param name="alfa1">
        /// The alfa 1.
        /// </param>
        /// <param name="alfa2">
        /// The alfa 2.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public abstract Task<SetParamEventArgs> SetParam(int bias, byte filterValue, int alfa1, int alfa2, CancellationToken token = default);

        /// <summary>
        /// Correlation function query version one.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public abstract Task<GetCorrelationEventArgs> GetCorrelationFunc(CancellationToken token = default);

        /// <summary>
        /// The start stop.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public abstract Task<StartStopEventArgs> Start_Stop(START_STOP value, CancellationToken token = default);

        /// <summary>
        /// The recognition request.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public abstract Task<SetParamEventArgs> RecognitionRequest(CancellationToken token = default);

        /// <summary>
        /// Create information field.
        /// </summary>
        /// <param name="codogramaLength">
        /// The codograma length.
        /// </param>
        /// <param name="cipher">
        /// The cipher.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        protected byte[] BaseCmd(int codogramaLength, byte cipher)
        {
            var bytesForSend = new byte[codogramaLength];
            bytesForSend[0] = this.AddrSender;
            bytesForSend[1] = this.AddrRecipient;
            bytesForSend[2] = cipher;
            this.CountCmd = Convert.ToByte((this.CountCmd == 255 ? 0 : this.CountCmd + 1));
            bytesForSend[3] = this.CountCmd;
            bytesForSend[4] = Convert.ToByte(bytesForSend.Length - 8);
            return bytesForSend;
        }

        /// <summary>
        /// Decoder of receive data.
        /// </summary>
        /// <param name="receiveBytes">
        /// The receive bytes.
        /// </param>
        protected abstract void DecodeReceiveData(byte[] receiveBytes);

        /// <summary>
        /// Converter byte[] to single
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <param name="i">
        /// The i.
        /// </param>
        /// <returns>
        /// The <see cref="float"/>.
        /// </returns>
        protected static float ToFloatNo(byte[] input, int i)
        {
            var newArray = new[] { input[i], input[i + 1], input[i + 2], input[i + 3] };
            return BitConverter.ToSingle(newArray, 0);
        }
    }
}
