﻿namespace UDP_Cuirasse
{
    public enum CodogramCiphers : byte
    {
        REQUEST_STATE = 1,
        SET_FREQ,
        REQUEST_SPECTRUM,
        REQUEST_CORRELATION_FUNCTION,
        SET_PARAM,
        START_STOP,
        RECOGNITION_REQUEST,
        DATA_FROM_RECOGNITION_SYSTEM
    }

    public enum START_STOP
    {
        STOP = 0,
        START
    }

    public enum ErrorCode : byte
    {
        SUCCESSFULLY = 0,
        UNSUCCESSFULLY = 1,
        UNKNOWN = 2
    }
    

    public enum ItemNumber : byte
    {
        CENTRAL = 0,
        LOCAL_1,
        LOCAL_2,
        LOCAL_3
    }

    public enum ReceiveMod : byte
    {
        Receiver,
        AsyncReceiver
    }

    public enum Optic : byte
    {
        LEAD_UP = 1,
        RADIO_INTELLIGENCE
    }
}
