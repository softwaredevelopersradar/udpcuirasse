﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using UDP_Cuirasse;

namespace WPF_UDP_Cuirasse
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MyIpBox.Text = "127.0.0.1";
            PortBox.Text = "15000";
            RemIpBox.Text = "127.0.0.1";
            RemPortBox.Text = "15001";
            ReadConfig();
        }

        private string myIp;
        private string myPort;
        private string remIp;
        private string remPort;

        private void ReadConfig()
        {
            try
            {
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}Config.txt"))
                {
                    myIp = sr.ReadLine();
                    myPort = sr.ReadLine();
                    remIp = sr.ReadLine();
                    remPort = sr.ReadLine();
                }
                MyIpBox.Text = myIp;
                PortBox.Text = myPort;
                RemIpBox.Text = remIp;
                RemPortBox.Text = remPort;
            }
            catch { }
        }

        private void CreateConfig()
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}Config.txt");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(MyIpBox.Text);
                    sw.WriteLine(PortBox.Text);
                    sw.WriteLine(RemIpBox.Text);
                    sw.WriteLine(RemPortBox.Text);
                }
            }
            catch { }
        }


        public enum WorkMod
        {
            RealTime,
            Emulator
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            modBox.Items.Add(ReceiveMod.Receiver);
            modBox.Items.Add(ReceiveMod.AsyncReceiver);
            Display.DisplayTime = false;
            FlagBox.Items.Add(WorkMod.RealTime);
            FlagBox.Items.Add(WorkMod.Emulator);
            FlagBox.SelectedIndex = 0;
            int[] i = { 0, 1, 2, 3, 4 };
            foreach (var a in i)
            {
                BandBox.Items.Add(a);
            }
            BandBox.SelectedIndex = 0;
            modBox.SelectedIndex = 0;
            StartStopBox.Items.Add("Stop");
            StartStopBox.Items.Add("Start");
            StartStopBox.SelectedIndex = 0;
            VersionCombo.Items.Add(0);
            VersionCombo.Items.Add(1);
            VersionCombo.Items.Add(2);
            VersionCombo.SelectedIndex = 0;
        }

        #region Commit
        //private void UDP_OnReceive(object sender, UDPClass.ReceiveEventArgs e)
        //{
        //    Display.AddTextToLog($"Receive: {e.Bytes.ToString()}\r\n");
        //    foreach (var i in e.Bytes)
        //    {
        //        Display.AddTextToLog(i.ToString());
        //    }
        //    Display.AddTextToLog("\r\n");
        //}
        #endregion

        private void UDP_OnSend(object sender, byte[] e)
        {
            string a = "";
            Display.AddTextToLog("Send: ", Brushes.Green);
            foreach (var i in e)
            {
                a += i.ToString("X2");
                a += " ";
            }
            if (e.Length <= 10000)
                Display.AddTextToLog(a, Brushes.Green);
        }

        private void UDP_OnDisconnect()
        {
            Dispatcher.Invoke(() => Display.AddTextToLog("Disconnect!"));
            uDP = null;
        }

        private void UDP_OnConnect()
        {
            Dispatcher.Invoke(() => Display.AddTextToLog("Connect!"));
        }

        UdpCuirasseModel uDP;
        
        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (uDP == null)
                {
                    uDP = new UDPClass(
                        MyIpBox.Text,
                        Convert.ToInt32(PortBox.Text),
                        RemIpBox.Text,
                        Convert.ToInt32(RemPortBox.Text),
                        4,
                        5);

                    bool a = uDP.Connect();
                    if (a)
                    {
                        this.UDP_OnConnect();
                    }
                    else
                    {
                        this.Display.AddTextToLog("Connect Error!", Brushes.Red);
                        return;
                    }
                    uDP.OnSendBytes += UDP_OnSend;
                    uDP.OnReceiveBytes += UDP_OnReceive;
                    uDP.OnDataFromRecognitionSystem += UDP_OnDataFromRecognitionSystem;
                    
                    CreateConfig();
                    #region Commit
                    //uDP.OnGetState += UDP_OnGetState;
                    //uDP.OnSetFreq += UDP_OnSetFreq;
                    //uDP.OnGetSpectrum += UDP_OnGetSpectrum;
                    //uDP.OnGetCorrelationFunc += UDP_OnGetCorrelationFunc;
                    //uDP.OnSetParam += UDP_OnSetParam;
                    //uDP.OnStartStop += UDP_OnStartStop;
                    #endregion
                }
            }
            catch { }
        }

        private void UDP_OnDataFromRecognitionSystem(object sender, DataRecognitionSystemEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Display.AddTextToLog($"Команда: {e.Cipher}");
                Display.AddTextToLog($"Код ошибки: {e.Error}");
                Display.AddTextToLog($"Количетсво пакетов: {e.Data.Count}");
            });
        }

        #region Commit
        //private void UDP_OnStartStop(object sender, StartStopEventArgs e)
        //{
        //    Dispatcher.Invoke(() =>
        //    {
        //        Display.AddTextToLog($"Команда: {e.Ciphers}");
        //        Display.AddTextToLog($"Состояние: {e.State}");
        //    });
        //}

        //private void UDP_OnSetParam(object sender, SetParamEventArgs e)
        //{
        //    Dispatcher.Invoke(() =>
        //    {
        //        Display.AddTextToLog($"Команда: {e.Ciphers}");
        //        Display.AddTextToLog($"Код ошибки: {e.Error}");
        //    });
        //}
        #endregion

        private void UDP_OnReceive(object sender, byte[] e)
        {
            string a = "";
            Dispatcher.Invoke(() =>
            { Display.AddTextToLog("Receive: ", Brushes.Red); });

            if (e[2] != 3 && e[2] != 4)
            {
                foreach (var i in e)
                {
                    a += i.ToString("X2");
                    a += " ";
                }

                if (e.Length < 100)
                    Dispatcher.Invoke(() =>
                    {
                        Display.AddTextToLog(a, Brushes.Red);
                    });
            }
            if(e[2] == 3)
                Dispatcher.Invoke(() =>
                {
                    a = "";
                    for (int i = 0; i < 12; i++)
                    {
                        a += e[i].ToString("X2");
                        a += " ";
                    }
                    Display.AddTextToLog(a, Brushes.Red);
                });
            else if (e[2] == 4)
                Dispatcher.Invoke(() =>
                {
                    a = "";
                    for (int i = 0; i < 11; i++)
                    {
                        a += e[i].ToString("X2");
                        a += " ";
                    }
                    Display.AddTextToLog(a, Brushes.Red);
                });
        }

        #region Commit
        //private void UDP_OnGetCorrelationFunc(object sender, GetCorrelationFuncEventArgs e)
        //{
        //    Dispatcher.Invoke(() =>
        //    {
        //        string c = "";

        //        foreach (var i in e.Inf)
        //        {
        //            if (i != 0)
        //                c += i.ToString("X");
        //        }
        //        int intValue = BitConverter.ToInt32(e.Inf,0);

        //        Display.AddTextToLog($"Команда: {e.Ciphers}");
        //        Display.AddTextToLog($"Длина информационного поля: {intValue}");
        //        Display.AddTextToLog($"Код ошибки: {e.Error}");
        //        Display.AddTextToLog($"A2: {Convert.ToDouble(e.Corr_1.sCorr)}");
        //        Display.AddTextToLog($"Размер первого массива амплитуд: {e.Corr_1.Corr.Count}");
        //        Display.AddTextToLog($"A1: {e.Corr_1.eCorr}");
        //        Display.AddTextToLog($"A3: {e.Corr_2.sCorr}");
        //        Display.AddTextToLog($"Размер второго массива амплитуд: {e.Corr_2.Corr.Count}");
        //        Display.AddTextToLog($"A1: {e.Corr_2.eCorr}");
        //        Display.AddTextToLog($"A4: {e.Corr_3.sCorr}");
        //        Display.AddTextToLog($"Размер третьего массива амплитуд: {e.Corr_3.Corr.Count}");
        //        Display.AddTextToLog($"A1: {e.Corr_3.eCorr}");
        //        Display.AddTextToLog($"A1: {e.Corr_4.sCorr}");
        //        Display.AddTextToLog($"Размер четвертого массива амплитуд: {e.Corr_4.Corr.Count}");
        //        Display.AddTextToLog($"A1: {e.Corr_4.eCorr}");
        //    });
        //}

        //private void UDP_OnSetFreq(object sender, SetFreqEventArgs e)
        //{
        //    Dispatcher.Invoke(() =>
        //    {
        //        Display.AddTextToLog($"Команда: {e.Ciphers}");
        //        Display.AddTextToLog($"Код ошибки: {e.Error}");
        //    });
        //}

        //private void UDP_OnGetSpectrum(object sender, GetSpectrumEventArgs e)
        //{
        //    Dispatcher.Invoke(() =>
        //    {
        //        string a = $"00 00 ";
        //        a += ((byte)e.Ciphers).ToString("X2");
        //        a += " ";

        //        a += e.Count.ToString("X2");
        //        a += " ";

        //        Array.Reverse(e.Inf);

        //        for (int i = 0; i < 4; i++)
        //        {
        //            a += e.Inf[i].ToString("X2");
        //            a += " ";
        //        }
        //        a += ((byte)e.Error).ToString("X2");
        //        a += " ";
        //        a += ((byte)e.Number).ToString("X2");
        //        Display.AddTextToLog(a, Brushes.Red);

        //        //var b = BitConverter.ToInt32(e.Inf, 1);


        //        string c = "";

        //        foreach (var i in e.Inf)
        //        {
        //            c += i.ToString("X");
        //        }

        //        int intValue = Convert.ToInt32(c, 16);

        //        Display.AddTextToLog($"Команда: {e.Ciphers}");
        //        Display.AddTextToLog($"Длина информационного поля: {intValue}");
        //        Display.AddTextToLog($"Код ошибки: {e.Error}");
        //        Display.AddTextToLog($"Номер пункта: {(byte)e.Number} - {e.Number}");
        //        Display.AddTextToLog($"Количество элементов: {e.Ampl.Count}");
        //    });
        //}

        //private void UDP_OnGetState(object sender, GetStateEventArgs e)
        //{
        //    Dispatcher.Invoke(() =>
        //    {
        //        Display.AddTextToLog($"Команда: {e.Ciphers}");
        //        Display.AddTextToLog($"Код ошибки: {e.Error}");
        //        Display.AddTextToLog($"Cостояние: {e.State}");
        //        Display.AddTextToLog($"Версия прошивки: {e.Version}");
        //    });
        //}
        #endregion

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                uDP.Disconnect();
                #region Commit
                //uDP.OnGetState -= UDP_OnGetState;
                ////uDP.OnSetFreq -= UDP_OnSetFreq;
                //uDP.OnGetSpectrum -= UDP_OnGetSpectrum;
                //uDP.OnGetCorrelationFunc -= UDP_OnGetCorrelationFunc;
                //uDP.OnSetParam -= UDP_OnSetParam;
                //uDP.OnStartStop -= UDP_OnStartStop;
                //uDP.OnReceive -= UDP_OnReceive;
                #endregion
            }
            catch { }
        }


        private async void GetStateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GetStateEventArgs a = await uDP.GetState();

                Dispatcher.Invoke(() =>
                {
                    Display.AddTextToLog($"Команда: {a.Ciphers}");
                    Display.AddTextToLog($"Код ошибки: {a.Error}");
                    Display.AddTextToLog($"Cостояние: {a.State}");
                    Display.AddTextToLog($"Версия прошивки: {a.Version}");
                });

            }
            catch { }
        }

        private async void SetFreqButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FreqBox.Text != null && WidthBox.Text != null && GainBox.Text != null)
                {
                    SetFreqEventArgs a = await uDP.SetFreq(Convert.ToInt32(FreqBox.Text), Convert.ToByte(GainBox.Text));

                    Dispatcher.Invoke(() =>
                    {
                        Display.AddTextToLog($"Команда: {a.Ciphers}");
                        Display.AddTextToLog($"Код ошибки: {a.Error}");
                    });

                }
            }
            catch { }
        }

        private async void GetSpectrumButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(BandBox.Text) >= 0 && Convert.ToInt32(BandBox.Text) <= 4)
                {
                    uDP.SetWaitTime(Convert.ToInt16(TimeBoxSpectrum.Text));
                    SW sw = new SW();
                    GetSpectrumEventArgs b = await uDP.GetSpectrum(Convert.ToByte(BandBox.Text));
                    sw.Stop();
                    Dispatcher.Invoke(() =>
                    {
                        SpectrumViewMethod(b);
                    });
                }
            }
            catch { }
        }

        private void SpectrumViewMethod(GetSpectrumEventArgs b)
        {
            #region Commit
            //string a = $"00 00 ";
            //a += ((byte)b.Ciphers).ToString("X2");
            //a += " ";

            //a += b.CountOfPackage.ToString("X2");
            //a += " ";

            //Array.Reverse(b.Inf);

            //for (int i = 0; i < 4; i++)
            //{
            //    a += b.Inf[i].ToString("X2");
            //    a += " ";
            //}
            //a += ((byte)b.Error).ToString("X2");
            //a += " ";
            //a += ((byte)b.Number).ToString("X2");
            //Display.AddTextToLog(a, Brushes.Red);

            //var b = BitConverter.ToInt32(e.Inf, 1);
            #endregion

            if (b == null)
            {
                Display.AddTextToLog($"Превышен лимит ожидания. Код команды 3");
                return;
            }

            string c = "";
            for (int i = b.Inf.Length - 1; i >= 0; i--)
            {
                c += b.Inf[i].ToString("X");
            }

            int intValue = Convert.ToInt32(c, 16);
            Display.AddTextToLog($"Количество пакетов: {b.CountOfPackage + 1}");
            Display.AddTextToLog($"Команда: {(byte)b.Ciphers} - {b.Ciphers}");
            Display.AddTextToLog($"Код ошибки: {b.Error}");
            Display.AddTextToLog($"Номер пункта: {(byte)b.Number} - {b.Number}");
            Display.AddTextToLog($"Количество элементов: {b.Ampl.Count}");
            Display.AddTextToLog($"Версия: {b.Version}");
        }

        private async void GetCorrelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                    uDP.SetWaitTime(Convert.ToInt16(TimeBoxCorr.Text));
                    
                    SW sw = new SW();
                    var a = await uDP.GetCorrelationFunc();
                    sw.Stop();
                    
                    Dispatcher.Invoke(() =>
                    {
                        CorrleViewMethodVersion1(a);
                    });
            }
            catch { }
        }

        private void CorrleViewMethodVersion1(GetCorrelationEventArgs a)
        {
            if(a == null)
                Display.AddTextToLog($"Превышен лимит ожидания");

            int lengthInf = 3;

            foreach (var item in a.Corr)
            {
                lengthInf += 16 + item.Corr.Count * 8;
            }
            Display.AddTextToLog($"Команда: {(byte)a.Ciphers} - {a.Ciphers}");
            Display.AddTextToLog($"Количество пакетов: {a.CountOfPackage + 1}");
            Display.AddTextToLog($"Длина информационного поля: {lengthInf}");
            Display.AddTextToLog($"Количество амплитуд: {(lengthInf - 3) / 8}");
            Display.AddTextToLog($"Код ошибки: {a.Error}");
            Display.AddTextToLog($"Версия: {a.Version}");
        }

        private void TimeBox_Checked(object sender, RoutedEventArgs e)
        {
            Display.DisplayTime = true;
        }

        private void TimeBox_Unchecked(object sender, RoutedEventArgs e)
        {
            Display.DisplayTime = false;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            #region Commit
            //uDP.OnGetStateT += UDP_OnGetState;
            //uDP.OnSetFreqT += UDP_OnSetFreq;
            //uDP.OnGetSpectrumT += UDP_OnGetSpectrum;
            //uDP.OnSetParamT += UDP_OnSetParam;
            //uDP.OnGetCorrelationFuncT += UDP_OnGetCorrelationFunc;
            //uDP.OnStartStopT += UDP_OnStartStop;
            #endregion
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            #region Commit
            //uDP.OnGetStateT -= UDP_OnGetState;
            //uDP.OnSetFreqT -= UDP_OnSetFreq;
            //uDP.OnGetSpectrumT -= UDP_OnGetSpectrum;
            //uDP.OnSetParamT -= UDP_OnSetParam;
            //uDP.OnGetCorrelationFuncT -= UDP_OnGetCorrelationFunc;
            //uDP.OnStartStopT -= UDP_OnStartStop;
            #endregion
        }

        private async void SetParamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SetParamEventArgs a = await uDP.SetParam(Convert.ToInt32(BiasBox.Text), Convert.ToByte(WidthBox.Text), Convert.ToInt32(Alfa1Box.Text), Convert.ToInt32(Alfa2Box.Text));

                Dispatcher.Invoke(() =>
                {
                    Display.AddTextToLog($"Команда: {a.Ciphers}");
                    Display.AddTextToLog($"Код ошибки: {a.Error}");
                });
            }
            catch { }
        }

        private async void StartStopButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StartStopEventArgs a = await uDP.Start_Stop((START_STOP)Convert.ToByte(StartStopBox.SelectedIndex));
                Dispatcher.Invoke(() =>
                {
                    Display.AddTextToLog($"Команда: {a.Ciphers}");
                    Display.AddTextToLog($"Состояние: {a.State}");
                });
            }
            catch { }
        }

        private void VersionCombo_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            try
            {
            }
            catch { }
        }

        private async void RecognitionRequestBut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var a = await uDP.RecognitionRequest();
                Dispatcher.Invoke(() =>
                {
                    Display.AddTextToLog($"Команда: {a.Ciphers}");
                    Display.AddTextToLog($"Код ошибки: {a.Error}");
                });
            }
            catch { }
        }
    }
}
